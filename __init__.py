# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import shipment
from . import configuration
from . import company


def register():
    Pool.register(
        shipment.ShipmentOut,
        shipment.ShipmentIn,
        shipment.ShipmentInternal,
        shipment.SendLinkStart,
        configuration.Configuration,
        company.Company,
        module='stock_shipment_web_link', type_='model')
    Pool.register(
        shipment.SendLink,
        module='stock_shipment_web_link', type_='wizard')
