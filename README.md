datalife_stock_shipment_web_link
================================

The stock_shipment_web_link module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_shipment_web_link/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_shipment_web_link)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
