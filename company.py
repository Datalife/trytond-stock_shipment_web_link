# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
import base64

__all__ = ['Company']


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    logo_base64 = fields.Function(fields.Binary('Logo base64'),
        'get_base64')
    background_logo_base64 = fields.Function(
        fields.Binary('Background logo base64'),
        'get_base64')

    def get_base64(self, name):
        return base64.b64encode(getattr(self, name[:-7], ""))
