# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

__all__ = ['Configuration']


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    shipment_link_url = fields.Char('Shipment link URL', required=True)

